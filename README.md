# dompack-masonry
clean and simple masonry using ES6 and dompack as only dependencies 


# Usage

This Masonry is set will some specific features:
- lightweight
- layout totally based on columns, so changes in CSS width's won't screw up layout
- only have dompack as dependancy


# Initialization through Javascript

Initialization is done though Javascript so you have total control on the order of initialization of page elements.

```
import DompackMasonry from "dompack-masonry";

// initialize in the DOMContentLoaded event or using dompack.register
dompack.register(".widget--widgetsgroup--masonry", (node, idx) =>
{
    masonry = new DompackMasonry(grid
            , { columns:  columns
              , gutter_x: 20
              , gutter_y: 20
              , items:    ".widget"
              //, equalizeheight: equalizeheight // We do equalizeheight using flexbox, because we don't need animating through abs.pos
              });
}
```


# Settings options in the DOM

Options can also be set through the DOM.
This can be usefull if you can have an arbitrary amount of masonries in a page which can have different settings.
You can then use a generic initialization and set some details that differ per masonry in the data-masonry attribute.
(NOTE: the setting specified in JS override the settings specified in the data-masonry attribute)

```
    <div class="wh-masonry" data-masonry='{"cols":3}'>...</div>

    <div class="wh-masonry" data-masonry='{"cols":3,"equalizeheight":true}'>...</div>
```


# Advanced use-cases

## Reorder items without changing the DOM order

The best way is to pass your own array of nodes in the 'items' option, in the order you want to use the items.
The position in the DOM will stay the same but the left/top will be changed, allowing you to perform CSS transitions.

## Nesting (a Masonry in a Masonry item)

This is possible if you use different CSS selectors/classnames in the option "items" when initializing the masonry.



# Future plans

- Support for positioning through column div's instead of absolute positioning. You will lose the ability to animate (using transitions), but any change in the masonry item's size will cause the browser to correct the positions of the widgets.

- A 'clear / nextline' trigger?
